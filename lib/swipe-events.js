import SwipeTypes from './swipeTypes'

function create() {
  const swipeEvents = {}
  const registerSwipeEvent = ($this, name, handler) => {
    const routeName = $this.routeName
    var events = swipeEvents[routeName]
    if (!events) {
      events = swipeEvents[routeName] = []
    }
    const eventHandler = async (evt) => {
      if ($this.$router.currentRoute.name !== routeName) {
        // 只触发当前路由下的事件
        return
      }
      if (evt.isCancel) {
        // 如果事件已被柱塞或onBeforeSwipe返回false，则取消触发事件
        return
      }
      if (handler && handler.isFunction) {
        if (handler.isAsync) {
          let res = await handler(evt)
          evt.isCancel = res === false
        } else {
          evt.isCancel = handler(evt) === false
        }
      }
    }
    $this.$root.$on(name, eventHandler)
    events.push({
      name: name,
      handler: eventHandler,
    })
  }

  return {
    data(){
      return {
        routeName: ''
      }
    },
    created() {
      this.routeName = this.$route.name
      if (this.onSwipeleft) {
        registerSwipeEvent(this, SwipeTypes.SwipeLeft, this.onSwipeleft)
      }
      if (this.onSwiperight) {
        registerSwipeEvent(this, SwipeTypes.SwipeRight, this.onSwiperight)
      }
      if (this.onSwipeup) {
        registerSwipeEvent(this, SwipeTypes.SwipeUp, this.onSwipeup)
      }
      if (this.onSwipedown) {
        registerSwipeEvent(this, SwipeTypes.SwipeDown, this.onSwipedown)
      }

      if (this.onBeforeSwipe) {
        registerSwipeEvent(this, SwipeTypes.Swipe, (evt) => {
          if (this.onBeforeSwipe(evt) === false) {
            evt.isCancel = true
          }
        })
      }
    },
    beforeDestroy() {
      var events = swipeEvents[this.routeName] || []
      events.forEach((evt) => {
        this.$root.$off(evt.name, evt.handler)
      })
      delete swipeEvents[this.routeName]
    },
    methods: {
      $checkParentElement(target, cb, maxCount) {
        // 检测指定DOM对象的父节点是否满足cb的条件
        if (!maxCount) {
          maxCount = 100
        }
        var counter = 0
        while (target && counter < maxCount) {
          if (target === document.body) {
            return false
          }
          var res = cb(target)
          if (res === true) {
            return true
          } else if (res === null) {
            return false
          }
          target = target.parentNode
          counter++
        }
      },
      $checkParentElementCssClass(evt, cssClasses) {
        // 检测指定DOM对象的父节点是否满足指定的css类名称
        if (!cssClasses) {
          return null
        }
        if (cssClasses.isString) {
          cssClasses = cssClasses.split(',')
        }
        return this.$checkParentElement(evt.target, (target) => {
          for (var i = 0; i < cssClasses.length; i++) {
            var css = cssClasses[i]
            if (target.classList.contains(css)) {
              return true
            }
          }
          return false
        })
      },
    },
  }
}

export default create()
