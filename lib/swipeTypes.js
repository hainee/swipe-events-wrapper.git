
let swipeTypes = {
  Swipe: 'swipe',
  SwipeLeft: 'swipeleft',
  SwipeRight: 'swiperight',
  SwipeUp: 'swipeup',
  SwipeDown: 'swipedown'
}

export default swipeTypes
