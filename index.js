import SwipeEventsWrapper from './index.vue'
import _swipeEvents from './lib/swipe-events'
import _swipeTypes from './lib/swipeTypes'
import VueTouch from 'vue-touch'
import Vue from 'vue'

Vue.use(VueTouch, {
  name: 'v-touch'
})

export default SwipeEventsWrapper
export const SwipeEvents = _swipeEvents
export const SwipeTypes = _swipeTypes
